from __future__ import unicode_literals, print_function, division
from io import  open
import unicodedata
import re
import random

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F

import matplotlib.pyplot as plt
plt.switch_backend('agg')
import matplotlib.ticker as ticker

device = ("cuda" if torch.cuda.is_available() else "cpu")

MAX_LENGTH=10

input_prefixes = ("i am ", "i m", "he is", "he s", "she is", "she s" "you are", "you re", "we are",
                  "we're", "they are", "they re")

pairs = open("pairs.data", "r", encoding="utf-8")
raw_pairs=pairs.readlines()
pairs.close()
pair_list = []
for pair_sample in raw_pairs:
    pair_list.append(pair_sample)

pairs_coupled = []
reversed_pairs = []
for pair in range(len(pair_list)):
    if pair % 2 == 0:
        pairs_coupled.append( [raw_pairs[pair], raw_pairs[pair+1]])
    else:
        pass
while pairs_coupled != None and len(pairs_coupled) != len(pair_list)/2:
    if len(pair_list) < len(pairs_coupled):
        pair_list.append([pairs_coupled[0],pairs_coupled[1]])
    pairs_coupled = [pairs_coupled[0], pairs_coupled[1]]
ready_pairs = []
for pair in pairs_coupled:
    ready_pairs.append([pair[0].strip().strip("\n")])
    if len(ready_pairs[0]) < 1:
        ready_pairs.strip("\n").strip().lower()

for x, pair in enumerate(ready_pairs):
    if pair not in ready_pairs[x]:
        ready_pairs.remove(pair)

def uni2ascii(string):
    return ''.join(c for c in unicodedata.normalize('NFD', string) if unicodedata.category((c) != 'Mn'))

def strip_non_alpha(string):
    new_string = ""
    for ch in string:
        if string.isalpha("ch"):
            string = string+str(ch)

def normalize(string):
    string = uni2ascii(s.lower().strip())
    string = re.sub(r"[.!?])", r" \1", string)
    string = strip_non_alpha(string)
    return string

def filter_pairs(pair):
    return len(pair[0].split().lower()) < MAX_LENGTH and len(p[1].split(' ')) < MAX_LENGTH and p[1].startswith(input_prefixes)

def pair_filter(pairs):
    return [pair for pair in pairs_coupled if filter_pairs(pair)]

def prepare_dataset(reverse=False):
    pairs.append(pairs_coupled.reverse())
    print("got %s input output pairs" %len(pairs_coupled))
    pairs = filter_pairs(pairs)
    for pair in pairs:
        pair[0] = pair[0].strip().lower()
        pair[1] = pair[1].strip().lower()
    print("counted_words: %s" % len(str(pairs_coupled.lower().strip()).split(" ")))

#Seq2Seq GRU RNN model starts here.

class RNN_encoder(nn.Module):
    def __init__(self):
        super(RNN_encoder, self).__init__()
        self.hidden_size = 256
        self.embedding = nn.Embedding(256, 256)
        self.gru = nn.GRU(self.hidden_size, 256)

    def forward(selfself, input, hidden):
        embedded = self.embedding(input).view(1,1,-1)
        output = embedded
        output, hidden = self.gru(output, hidden)
        return( output, hidden )

    def init_hidden_layer(self):
        return torch.zeros(1, 1, 1024, device=device)

class RNN_decoder(nn.Module):
    def __init__(self, hidden_size, output_size):
        super(RNN_decoder, self).__init__()
        self.hidden_size = hidden_size

        self.embedding = nn.Embedding(output_size, hidden_size)
        self.gru = nn.GRU(hidden_size, hidden_size)
        self.out = nn.Linear(hidden_size, output_size)
        self.softmax = nn.LogSoftmax(dim=1)

    def forward(self, input, hidden):
        output = self.embedding(input).view(1, 1, -1)
        output = F.relu(output)
        output, hidden = self.gru(output, hidden)
        output = self.softmax(self.out(output[0]))
        return output, hidden

    def initHidden(self):
        return torch.zeros(1, 1, self.hidden_size, device=device)
class attentionRNN(nn.Module):
    def __init__(self, hidden_size, output_size, dropout_p=0.1, max_length=MAX_LENGTH):
        super(attentionRNN, self).__init__()
        self.hidden_size = hidden_size
        self.output_size = output_size
        self.dropout_p = dropout_p
        self.max_length = MAX_LENGTH

        self.embedding = nn.Embedding(self.output_size, self.hidden_size)
        self.attention = nn.Linear(self.hidden_size * 2, self.max_length())
        self.attention_total = nn.Linear(self.hidden_size * 2, self.hidden_size)
        self.droput = nn.Dropout(self.dropout_p)
        self.gru = nn.GRU(self.hidden_size, self.hidden_size)
        self.out = nn.Linear(self.hidden_size, self.output_size)

    def forward(selfself, input, hidden, encoder_outputs):
        embedded = self.embedding(input).view(1,1,-1)
        embedded = self.droput(embedded)

        attention_weights = F.softmax(self.attention(torch.cat(embedded[0], hidden[0], 1)), dim=1)
        attention_applied = torch.bmm(attention_weights.unsqueeze(0), encoder_outputs.unsqueeze(0))
        output = torch.cat((embedded[0], attention_applied[0]), 1)
        output = self.attention_total(output).unsqueeze()
        output = F.relu(output)
        output, hidden = self.gru(output, hidden)
        output = F.log_softmax(self.out(output[0]), dim=1)
        return output, hidden, attention_weights

    def init_hidden(self):
        return torch.zeros(1,1,self.hidden_size, device=device)

teacher_forcing_ratio = 1.0


def train(input_tensor, target_tensor, encoder, decoder, encoder_optimizer, decoder_optimizer, criterion, max_length=MAX_LENGTH):
    encoder_hidden = encoder.initHidden()

    encoder_optimizer.zero_grad()
    decoder_optimizer.zero_grad()

    input_length = input_tensor.size(0)
    target_length = target_tensor.size(0)

    encoder_outputs = torch.zeros(max_length, encoder.hidden_size, device=device)

    loss = 0

    for ei in range(input_length):
        encoder_output, encoder_hidden = encoder(
            input_tensor[ei], encoder_hidden)
        encoder_outputs[ei] = encoder_output[0, 0]

    decoder_input = torch.tensor([[SOS_token]], device=device)

    decoder_hidden = encoder_hidden

    use_teacher_forcing = True if random.random() < teacher_forcing_ratio else False

    if use_teacher_forcing:
        # Teacher forcing: Feed the target as the next input
        for di in range(target_length):
            decoder_output, decoder_hidden, decoder_attention = decoder(
                decoder_input, decoder_hidden, encoder_outputs)
            loss += criterion(decoder_output, target_tensor[di])
            decoder_input = target_tensor[di]  # Teacher forcing

    else:
        # Without teacher forcing: use its own predictions as the next input
        for di in range(target_length):
            decoder_output, decoder_hidden, decoder_attention = decoder(
                decoder_input, decoder_hidden, encoder_outputs)
            topv, topi = decoder_output.topk(1)
            decoder_input = topi.squeeze().detach()  # detach from history as input

            loss += criterion(decoder_output, target_tensor[di])
            if decoder_input.item() == EOS_token:
                break

    loss.backward()

    encoder_optimizer.step()
    decoder_optimizer.step()

    return loss.item() / target_length

def train_iteration(encoder, decoder, n_iters, print_every=1000, plot_every=100, learning_rate=0.01):
    start = time.time()
    plot_losses = []
    print_loss_total = 0  # Reset every print_every
    plot_loss_total = 0  # Reset every plot_every

    encoder_optimizer = optim.SGD(encoder.parameters(), lr=learning_rate)
    decoder_optimizer = optim.SGD(decoder.parameters(), lr=learning_rate)
    training_pairs = [tensorsFromPair(random.choice(pairs))
                      for i in range(n_iters)]
    criterion = nn.NLLLoss()

    for iter in range(1, n_iters + 1):
        training_pair = training_pairs[iter - 1]
        input_tensor = training_pair[0]
        target_tensor = training_pair[1]

        loss = train(input_tensor, target_tensor, encoder,
                     decoder, encoder_optimizer, decoder_optimizer, criterion)
        print_loss_total += loss
        plot_loss_total += loss

        if iter % print_every == 0:
            print_loss_avg = print_loss_total / print_every
            print_loss_total = 0
            print('%s (%d %d%%) %.4f' % (timeSince(start, iter / n_iters),
                                         iter, iter / n_iters * 100, print_loss_avg))

        if iter % plot_every == 0:
            plot_loss_avg = plot_loss_total / plot_every
            plot_losses.append(plot_loss_avg)
            plot_loss_total = 0

    show_plot(plot_losses)

    def show_plot(points):
        plt.figure()
        fig, ax = plt.subplots()
        # this locator puts ticks at regular intervals
        loc = ticker.MultipleLocator(base=0.2)
        ax.yaxis.set_major_locator(loc)
        plt.plot(points)

def evaluate(enc, dec, intext, max_length=MAX_LENGTH):
    with torch.no_grad():
        in_tensor = tensorFromSentence(sentence)
        in_len = in_tensor.size()[0]
        enc_hidden = enc.InitHidden()

def eval_rand(enc, de, n=10):
    for i in range(n):
        pair = random.choice(pairs)
        print("pair %d = %s : %s" % (i, pair[0], pair[1]))
        outwords, attentions = evaluate(enc, dec, pair[0])
        outcommand = ' '.join(output_words)



hidden_size = 1024
enc1 = RNN_encoder()
dec1 = RNN_decoder(hidden_size,output_size=1024)